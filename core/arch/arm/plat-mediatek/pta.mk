define my-dir
$(patsubst %/,%,$(dir $(lastword $(MAKEFILE_LIST))))
endef

define all-pta-mk-under
$(wildcard $1/*/pta.mk)
endef

define _add-prebuilt-libname
libnames += $$(strip $1)
libdirs += $$(strip $2)
libdeps += $$(strip $2)/lib$$(strip $1).a
LDADD += --start-group --whole-archive -l$$(strip $1) --no-whole-archive -L$$(strip $2) --end-group
endef

define add-prebuilt-libname
$(eval $(call _add-prebuilt-libname, $1, $2))
endef

ifeq ($(strip $(SOC_PLATFORM)),mt8183)
$(call add-prebuilt-libname,efuse,$(LIBDIR))
endif

ifeq ($(strip $(SOC_PLATFORM)),mt8365)
$(call add-prebuilt-libname,efuse,$(LIBDIR))
endif

ifeq ($(strip $(SOC_PLATFORM)),mt8195)
$(call add-prebuilt-libname,efuse,$(LIBDIR))
endif
